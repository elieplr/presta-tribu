<?php

if (!defined('_PS_VERSION_')) {
    exit;
}
class TribufaqQuestion extends ObjectModel
{
    public $id_tribufaq_question;
    public $id_tribufaq_category;
    public $question;
    public $response;
    public $active;
    public $date_add;

    public static $definition = [
        'table'     => 'tribufaq_question',
        'primary'   => 'id_tribufaq_question',
        'multilang' => true,
        'fields'    => [
            'id_tribufaq_category' => ['type' => self::TYPE_INT, 'required' => true],
            'question'             => ['type' => self::TYPE_STRING, 'required' => true, 'lang' => true, 'validate' => 'isGenericName'],
            'response'             => ['type' => self::TYPE_HTML, 'required' => true, 'lang' => true, 'validate' => 'isCleanHtml'],
            'active'               => ['type' => self::TYPE_BOOL, 'validate' => 'isBool', 'active' => 'status'],
            'date_add'             => ['type' => self::TYPE_DATE, 'required' => false]
        ],
    ];

    public function getLastFaq($number)
    {
        $query = new DbQuery();
        $query->from('tribufaq_question', 'faq');
        $query->select('*');
        $query->leftJoin('tribufaq_question_lang', 'faql', 'faq.id_tribufaq_question = faql.id_tribufaq_question AND faql.id_lang=' . Context::getContext()->language->id);
        $query->where('faq.active = 1 ');
        $query->orderBy('date_add ASC');
        $query->limit((int)$number);
        return Db::getInstance()->executeS($query);
    }

    public function formatLastFaq($faqs):array
    {
        $formatted_array = array();

        // Boucle pour formater l'array
        foreach ($faqs as $question) {
            $category_id = $question["id_tribufaq_category"];

            // Si la catégorie n'est pas active, on passe à la suivante
            if (TribufaqCategory::getCategoryIsActive($category_id) == 0) {
                continue;
            }

            // Si la catégorie n'existe pas encore dans l'array formaté, créez-la
            if (!isset($formatted_array[$category_id])) {
                $category_name = TribufaqCategory::getCategoryName($category_id);
                $formatted_array[$category_id] = array(
                    "category_name" => $category_name,
                    "questions" => array()
                );
            }

            // Ajoutez la question et la réponse à la catégorie appropriée
            $formatted_array[$category_id]["questions"][] = array(
                "question" => $question["question"],
                "response" => $question["response"]
            );
        }

        return array_values($formatted_array);
    }
}