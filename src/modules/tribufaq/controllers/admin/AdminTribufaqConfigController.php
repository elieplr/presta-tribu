<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class AdminTribufaqConfigController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();

        $this->bootstrap = true;
        $this->table = 'configuration';
        $this->identifier = 'name';
        $this->className = 'Configuration';
        $this->context = Context::getContext();
        $this->fields_options = array(
            'general' => array(
                'title' => $this->module->l('Paramètres généraux'),
                'fields' => array(
                    'TRIBUFAQ_HOME_QA_COUNT' => array(
                        'title' => $this->module->l('Nombre de questions/réponses sur la page d\'accueil'),
                        'desc' => $this->module->l('Définissez le nombre de questions/réponses à afficher sur la page d\'accueil.'),
                        'cast' => 'intval',
                        'validation' => 'isUnsignedInt',
                        'type' => 'text',
                    ),
                ),
                'submit' => array('title' => $this->module->l('Enregistrer')),
            ),
        );
    }

    public function renderForm()
    {
        $fields_form = array(
            'legend' => array(
                'title' => $this->l('Paramètres généraux'),
                'icon' => 'icon-cogs',
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Nombre de questions/réponses sur la page d\'accueil'),
                    'name' => 'TRIBUFAQ_HOME_QA_COUNT',
                    'size' => 20,
                    'required' => true,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Enregistrer'),
                'class' => 'btn btn-default pull-right',
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit' . $this->table;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->module->name . '&tab_module=' . $this->module->tab . '&module_name=' . $this->module->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
        );

        return $helper->generateForm(array($fields_form));
    }

    protected function getConfigFieldsValues()
    {
        return array(
            'TRIBUFAQ_HOME_QA_COUNT' => Configuration::get('TRIBUFAQ_HOME_QA_COUNT'),
        );
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submit' . $this->table)) {
            $errors = array();

            $homeQaCount = Tools::getValue('TRIBUFAQ_HOME_QA_COUNT');

            if (!Validate::isUnsignedInt($homeQaCount)) {
                $errors[] = $this->module->l('Invalid value for home Q&A count.');
            } else {
                Configuration::updateValue('TRIBUFAQ_HOME_QA_COUNT', $homeQaCount);
            }

            if (empty($errors)) {
                $this->confirmations[] = $this->module->l('Settings updated successfully.');
            } else {
                foreach ($errors as $error) {
                    $this->errors[] = $error;
                }
            }
        }

        parent::postProcess();
    }
}
